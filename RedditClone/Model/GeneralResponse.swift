//
//  GeneralResponse.swift
//  RedditClone
//
//  Created by Ernesto Jose Contreras Lopez on 15/5/23.
//

import Foundation

struct GeneralResponse: Codable {
    let data: PostResponse
}

struct PostResponse: Codable {
    let children: [Post]
}

struct Post: Codable {
    var data: PostData
}

struct PostData: Codable {
    let subreddit: String?
    let title: String?
    let subredditNamePrefixed: String?
    let tag: String?
    let description: String?
    let downs, ups: Int?
    let thumbnailHeight: Int?
    let name: String?
    let subredditType: String?
    let totalAwardsReceived: Int?
    let thumbnailWidth: Int?
    let score: Int?
    let thumbnail: String?
    let preview: Preview?
    let subredditID: String?
    let author: String?
    let numComments: Int?
    var url: String?
    let subredditSubscribers: Int?
    let createdUTC: Double?
    let isVideo: Bool?
    
    mutating func createPreviewURL() {
        guard let preview = preview?.images.first?.source.url else {
            return
        }
        self.url = String(htmlEncodedString: preview)
    }
    
    func stringScore() -> String? {
        guard let score = self.score else {
            return nil
        }
        return String(score)
    }
    
    func stringComments() -> String? {
        guard let comments = self.numComments else {
            return nil
        }
        return String(comments)
    }

    enum CodingKeys: String, CodingKey {
        case subreddit
        case title
        case tag = "link_flair_text"
        case description = "selftext"
        case subredditNamePrefixed = "subreddit_name_prefixed"
        case downs, ups
        case thumbnailHeight = "thumbnail_height"
        case name
        case subredditType = "subreddit_type"
        case totalAwardsReceived = "total_awards_received"
        case thumbnailWidth = "thumbnail_width"
        case score
        case thumbnail
        case preview
        case subredditID = "subreddit_id"
        case author
        case numComments = "num_comments"
        case url
        case subredditSubscribers = "subreddit_subscribers"
        case createdUTC = "created_utc"
        case isVideo = "is_video"
    }
}

// MARK: - Preview
struct Preview: Codable {
    let images: [Image]
    let enabled: Bool
}

// MARK: - Image
struct Image: Codable {
    let source: Source
    let resolutions: [Source]
    let id: String
}

// MARK: - Source
struct Source: Codable {
    let url: String
    let width, height: Int
}
