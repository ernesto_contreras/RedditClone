//
//  Extensions.swift
//  RedditClone
//
//  Created by Ernesto Jose Contreras Lopez on 15/5/23.
//

import Foundation

extension Dictionary {
    func objectVersion<T: Codable>(type: T.Type) -> T? {
        let data = try? JSONSerialization.data(withJSONObject: self)
        let decodedData = try? JSONDecoder().decode(type, from: data ?? Data())
        return decodedData
    }
}

extension Notification.Name {
    static let retrievePosts = Notification.Name("retrievePosts")
}

extension DateFormatter {
    static func unixToStringDate(_ unix: Double?) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        if let unix = unix {
            return dateFormatter.string(from: Date(timeIntervalSince1970: unix))
        } else {
            return nil 
        }
    }
}


extension String {

    init?(htmlEncodedString: String) {

        guard let data = htmlEncodedString.data(using: .utf8) else {
            return nil
        }

        let options: [NSAttributedString.DocumentReadingOptionKey: Any] = [
            .documentType: NSAttributedString.DocumentType.html,
            .characterEncoding: String.Encoding.utf8.rawValue
        ]

        guard let attributedString = try? NSAttributedString(data: data, options: options, documentAttributes: nil) else {
            return nil
        }

        self.init(attributedString.string)

    }

}
