//
//  HomeViewModel.swift
//  RedditClone
//
//  Created by Ernesto Jose Contreras Lopez on 15/5/23.
//

import Foundation
import SDWebImage

class HomeViewModel {
    
    var didFailed: ((String?) -> Void)?
    private var posts: [Post]?
    
    init() {
        requestOauth()
        addObserver()
    }
    
    func getPosts() -> [PostData] {
        self.posts?.map({$0.data}) ?? []
    }
    
    private func addObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(handlePostsRequest), name: .retrievePosts, object: nil)
    }
    
    private func requestOauth() {
        if !Helper.isAuthenticated() {
            NetworkManager.shared.requestOauthPermission()
        } else {
            if !Helper.hasToken() {
                NetworkManager.shared.requestToken { isSuccess in
                    if isSuccess {
                        self.requestList()
                    } else {
                        print("T3ST failed isSuccess", isSuccess)
                    }
                }
            } else {
                requestList()
            }
        }
    }
    
    private func requestList() {
        NetworkManager.shared.request(api: API.topPosts, type: GeneralResponse.self, headers: NetworkManager.tokenHeader) { result in
            switch result {
            case .success(let response):
                self.posts = response?.data.children
                self.cachePreviews()
            case .failure(let error):
                self.didFailed?(error.localizedDescription)
            }
        }
    }
    
    private func cachePreviews() {
        var previews: [URL] = []
        
        if let posts = self.posts {
            for index in posts.indices {
                self.posts?[index].data.createPreviewURL()
                if let urlString = self.posts?[index].data.url,
                   let url = URL(string: urlString) {
                    previews.append(url)
                }
            }
        }

        SDWebImagePrefetcher.shared.prefetchURLs(previews, progress: { _, _ in
        }) { (noOfFinishedUrls, noOfSkippedUrls) in
            self.didFailed?(nil)
        }
    }
    
    @objc private func handlePostsRequest(_ notification: Notification) {
        requestList()
    }
}
